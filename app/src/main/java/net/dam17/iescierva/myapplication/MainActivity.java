package net.dam17.iescierva.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.databinding.DataBindingUtil;
import android.view.View;
import android.widget.Button;

import net.dam17.iescierva.myapplication.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding  b;
    boolean pulsado = true;
    CharSequence varaux;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b = DataBindingUtil.setContentView(this, R.layout.activity_main);
        varaux = b.displayTextView.getText();

        final View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button pulsa = (Button)  v;
                String c = pulsa.getText().toString();


                if (pulsado){

                    b.displayTextView.setText("Te cambio");
                    pulsado = false;


                }else {
                    b.displayTextView.setText(varaux);
                    pulsado = true;

                }

            }


        };

        b.buttonPress.setOnClickListener(listener);
    }
}
